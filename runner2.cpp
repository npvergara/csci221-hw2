#include <iostream>
#include "Decrypt.h"
#include <string>
#include <unistd.h>
using namespace std;

int main(int argc, char** argv){
 char opt;
 char *inFile = "defaultInput.txt";
 char *outFile = "defaultOutput.txt";
 int iflag, oflag, eflag, dflag, cflag;
 iflag = 0;
 oflag = 0;
 eflag = 0;
 dflag = 0;
 cflag = 0;
 bool working = true;
 while((opt = getopt(argc, argv, "i:o:edc")) != -1){
  switch(opt){
   case 'i':
    iflag = 1;
    inFile = optarg;
    break;
   case 'o':
    oflag = 1;
    outFile = optarg;
    break;
   case 'e':
    eflag = 1;
    break;
   case 'd':
    dflag = 1;
    break;
   case 'c':
    cflag = 1;
    break;
   default:
    working = false;
    cout << "ERROR" << endl;
    cout << "Usage: ./<runner_name> <options> -i <input_file_name> -o <output_file>" << endl;
    cout << "Options: [-e to ENCYRYPT] [-d to DECRYPT] [-c to CRACK WITH BRUTE FORCE]" << endl;
    break;
  }
 }

 if(argc != 6 || (iflag == 0) || (oflag == 0) || (eflag == 0 && dflag == 0 && cflag == 0)){
   working = false;
   cout << "ERROR" << endl;
   cout << "Usage: ./<runner_name> <options> -i <input_file_name> -o <output_file>" << endl;
   cout << "Options: [-e to ENCYRYPT] [-d to DECRYPT] [-c to CRACK WITH BRUTE FORCE]" << endl;
 }

 //Actual output to user when using the program
 if(working){
  if(dflag == 1){
    cout << "Welcome to the Permutation Cipher Program: DECRYPTION MODE" << endl << endl;
    cout << "Input file: " << inFile << endl;
    cout << "Output file: " << outFile << endl << endl;
    string text = "";
    //Gets the line of text from the file
    fstream inputFile(inFile, ios::in);
    if(inputFile.is_open()){
     getline(inputFile, text);
     inputFile.close();
    }
    else{
     cout << "ERROR: Could not open input file" << endl;
    }
    //Encrypts the texts and stores it as a string
    string decryptedText = decrypt(text);
    //Shows encrypted text and puts it into the output file
    cout << "Decrypted Text: " << decryptedText << endl;
    fstream outputFile(outFile, ios::out);
    if(outputFile.is_open()){
     outputFile << decryptedText;
     outputFile.close();
    }
    else{
     cout << "ERROR: Could not open output file" << endl;
    }
  }
  else{
   cout << "ERROR: Unknown flags" << endl;
  }
 }
 return 0;
}

