#include "Encrypt.h"
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

string encrypt(string textToEncrypt){
 //Gets the information necessary for the cipher from the user
 int blockSize = 0;
 cout << "Please enter the block size (2-6)" << endl;
 cin >> blockSize;
 cout << endl;
 int permutation[blockSize];
 cout << "Please enter the permuation order with spaces in between the digits" << endl;
 cout << "(i.e. Block:[4]  Permutation:[2 4 3 1])" << endl;
 for(int i = 0; i < blockSize; i++){
  cin >> permutation[i];
 }

 //Encryption: fills in any empty space in the block
 string toEncrypt = textToEncrypt;
 if(!(toEncrypt.length()%blockSize == 0)){
  for(int i = 0; i < toEncrypt.length()%blockSize; i++){
   toEncrypt += "*";
  }
 }
 //Encryption: shuffles the text according to the permutation
 int counter = 0;
 char encryptedText[toEncrypt.length()];
 int placeHolder = 0;
 char chunk[blockSize];
 while(counter < toEncrypt.length()){
  //gets the chunk of text that needs to be shuffled in the block
  for(int i = 0; i < blockSize; i++){
   chunk[i] = toEncrypt.at(counter);
   counter++;
  }
  //shuffles the chunk of text and adds it to the final string
  for(int j = 0; j < blockSize; j++){
   encryptedText[placeHolder] = chunk[(permutation[j]-1)];
   placeHolder++;
  }
 }

 //Converts the array of chars into a string and returns the encrypted string
 string output = "";
 for(int i = 0; i < toEncrypt.length(); i++){
  output += string(1, encryptedText[i]);
 }
 return output; 
}
