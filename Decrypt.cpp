#include "Decrypt.h"
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

string decrypt(string textToDecrypt){

 string toDecrypt = textToDecrypt;
 string decryptedText = "";

 //Gets the information necessary for the cipher from the user
 int blockSize = 0;
 cout << "Please enter the block size (2-6)" << endl;
 cin >> blockSize;
 cout << endl;
 int permutation[blockSize];
 cout << "Please enter the permuation order with spaces in between the digits" << endl;
 cout << "(i.e. Block:[4]  Permutation:[2 4 3 1])" << endl;
 for(int i = 0; i < blockSize; i++){
  cin >> permutation[i];
 }

 //Decryption
 int counter = 0;
 int placeHolder = 0;
 char decryptChars[toDecrypt.length()];
 char chunk[blockSize];
 while(counter < toDecrypt.length()){
  //Gets chunk of encrypted text
  for(int i = 0; i < blockSize; i++){
   chunk[i] = toDecrypt.at(counter);
   counter++;
  }
  //Unshuffles according to permutation
  for(int j = 0; j < blockSize; j++){
   decryptChars[placeHolder+(permutation[j]-1)] = chunk[j];
  }
  placeHolder += (blockSize);
 }

 //Turns array of chars into string and gets rid of filler
 for(int i = 0; i < toDecrypt.length(); i++){
   if(!(decryptChars[i] == '*')){
   decryptedText += string(1, decryptChars[i]);
  }
 }
 return decryptedText;
}
