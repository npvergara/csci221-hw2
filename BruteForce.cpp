#include "BruteForce.h"
#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
using namespace std;

string bruteForce(string textToForce){
 string toForce = textToForce;
 string attempt = "";
 char attemptChars[toForce.length()];
 int filler = 0;

 //Counts the number of fillers
 for(int i = 0; i < toForce.length(); i++){
  if(toForce.at(i) == '*'){
   filler++;
  }
 }

 //Tries to decrypt until a result where all the fillers are at the end
 bool done = false;
 while(!done){
  //Generates a random block size and permutation
  int blockSize = 0;
  //Checks to see if the block size is evenly divisible by the length of the string
  bool checkBlockSize = false;
  for(int z = 0; z < 25; z++){
   blockSize = rand()%4+2;
   if(toForce.length()%blockSize == 0){
    checkBlockSize = true;
   }
   else{
    blockSize = rand()%5+2;
   }
  }
  int permutation[blockSize];
  for(int i = 0; i < blockSize; i++){
   int randPerm = rand()%blockSize;
   //Makes sure the random number generated has not been made yet
   for(int j = 0; j < blockSize; j++){
    if(randPerm == permutation[j]){
     int temp = randPerm;
     //Generates a new random number between the old randPerm and blockSize
     randPerm = rand()%blockSize;
    }
   }
   permutation[i] = randPerm;
  }
  //Decrypts with block size and permutation
  int counter = 0;
  int placeHolder = 0;
  char decryptChars[toForce.length()];
  char chunk[blockSize];
  while(counter < toForce.length()){
   //Gets chunk of encrypted text
   for(int k = 0; k < blockSize; k++){
    chunk[k] = toForce.at(counter);
    counter++;
   }
   //Unshuffles according to permutation
   for(int l = 0; l < blockSize; l++){
    decryptChars[placeHolder+(permutation[l])] = chunk[l];
   }
   placeHolder += (blockSize);
  }
  //Checks to see if the last characters of the array are the fillers
  for(int m = 0; m < filler; m++){
   int check = filler-m;
   if(decryptChars[toForce.length()-check] == '*'){
    done = true;
    //Fills in attemptChars with the completed decryptChars
    for(int y = 0; y < toForce.length(); y++){
     attemptChars[y] = decryptChars[y];
    }
   }
   else{
    done = false;
   }
  }
 }

 //Turns array of chars into string and gets rid of filler
 for(int x = 0; x < toForce.length(); x++){
  //if(!(attemptChars[x] == '*')){
   attempt += string(1, attemptChars[x]);
  //}
 }

 return attempt;
}
