#include <iostream>
#include "Encrypt.h"
#include <string>
//#include <ctype.h>
//#include <stdio.h>
//#include <stdlib.h>
#include <unistd.h>
using namespace std;

int main(int argc, char** argv){
 char opt;
 char *inFile = "defaultInput.txt";
 char *outFile = "defaultOutput.txt";
 int iflag, oflag, eflag, dflag, cflag;
 iflag = 0;
 oflag = 0;
 eflag = 0;
 dflag = 0;
 cflag = 0;
 char choice = 'x';
 bool working = true;
 while((opt = getopt(argc, argv, "i:o:edc")) != -1){
  switch(opt){
   case 'i':
    iflag = 1;
    inFile = optarg;
    break;
   case 'o':
    oflag = 1;
    outFile = optarg;
    break;
   case 'e':
    eflag = 1;
    choice = 'e';
    break;
   case 'd':
    dflag = 1;
    choice = 'd';
    break;
   case 'c':
    cflag = 1;
    choice = 'c';
    break;
   default:
    working = false;
    cout << "ERROR" << endl;
    cout << "Usage: ./<runner_name> <options> -i <input_file_name> -o <output_file_name>" << endl;
    cout << "Options: [-e to ENCYRYPT] [-d to DECRYPT] [-c to CRACK WITH BRUTE FORCE]" << endl;
    break;
  }
 }

 if(argc != 6 || (iflag == 0) || (oflag == 0) || (eflag == 0 && dflag == 0 && cflag == 0)){
   working = false;
   cout << "ERROR" << endl;
   cout << "Usage: ./<runner_name> <options> -i <input_file_name> -o <output_file_name>" << endl;
   cout << "Options: [-e to ENCYRYPT] [-d to DECRYPT] [-c to CRACK WITH BRUTE FORCE]" << endl;
 }


 //Actual output to user when using the program
 if(working){
  if(eflag == 1){
    cout << "Welcome to the Permutation Cipher Program: ENCRYPTION MODE" << endl << endl;
    cout << "Input file: " << inFile << endl;
    cout << "Output file: " << outFile << endl << endl; 
    string text = "";
    //Gets the line of text from the file
    fstream inputFile(inFile, ios::in);
    if(inputFile.is_open()){
     getline(inputFile, text);
     inputFile.close();
    }
    else{
     cout << "ERROR: Could not open input file" << endl;
    }
    //Encrypts the texts and stores it as a string
    string encryptedText = encrypt(text);
    //Shows encrypted text and puts it into the output file
    cout << "Encrypted Text: " << encryptedText << endl;
    fstream outputFile(outFile, ios::out);
    if(outputFile.is_open()){
     outputFile << encryptedText;
     outputFile.close();
    }
    else{
     cout << "ERROR: Could not open output file" << endl;
    }
  }
  else{
   cout << "ERROR: Unknown flags" << endl;
  }
 }
 return 0;
}
