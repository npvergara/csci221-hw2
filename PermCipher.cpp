#include "Encrypt.h"

//Defining the encryption method
string encrypt(int block, string perm, string text){
 string encryptedText = "";
 //Breaks the permutation into an array of ints
 int[] permutation = new int[block]
 for(int i = 0; i < perm.length(); i++){
  permutation[i] = (perm.at(i) - '0');
 }
 //If the length of the string is not evenly divisible with the block
 //it will fill in the empty spaces with *'s
 int textLen = text.length();
 if(!(textLen%block == 0)){
  for(int i = 0; i < textLen%block; i++){
   text += "*";
  } 
 }
 //Encrypts the text now
 char[] chunk = new char[block];
 char[] cryptChunk = new char[block];
 int count = 0;
 //Will repeat the process for each block in the string
 for(int i = 0; i < (text.length()/block); i++){
  //Fills in each chunk with the appropriate characters
  for(int j = 0; j < block; j++){
   chunk[j] = text.at(count);
   count++;
  }
  //Shuffles the chunk letters according to permutation
  for(int k = 0; k < block; k++){
   cryptChunk[k] = chunk[permutation[k]];
  }
  //Adds everything from cryptChunk into the final string
  for(int l = 0; l < std::size(cryptChunk); l++){
   encryptedText += cryptChunk[l];
  }
 }
 return encryptedText;
}
