#include <iostream>
#include <fstream>
#include "Encrypt.h"
using namespace std;

//When encryption is chosen, it runs the following
void encryption(string inputFile, string outputFile){
 cout << "Welcome to the Permutation Cipher Program: Encryption Mode" << endl;
 cout << endl;
 int blockSize;
 string permutation;
 string text;
 cout << "Input into: " << inputFile << endl;
 cout << "Output into: " << outputFile << endl;
 cout << "Please enter the size of the block (2-6)";
 cin >> blockSize;
 cout << "Please enter the permutation (i.e. Block:4 Permutation:2314)";
 cin >> permutation;
 cout << "Please enter the text you would like to encrypt:" << endl;
 cin >> text;
 string encryptedText = encrypt(blockSize, permutation, text);
 cout << "Encrypted Text: " + encryptedText;
}

int main(int argc, char** argv){
 char opt;
 string inFile;
 string outFile;
 bool iflag = false;
 bool oflag = false;
 bool eflag = false;
 while((opt = getopt(argc, argv, "i:o:e")) != -1){
  switch(opt){
   case 'i':
    iflag = true;
    inFile = optarg;
    break;
 }
}
